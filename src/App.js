import { useState, useEffect } from 'react';
import './App.css';
import Navbar from './components/AppNavbar';
import Landing from './pages/Home';
//import Footer from './components/Footer';
import Courses from './pages/Catalog';
import CourseView from './pages/CourseView'
import Login from './pages/Login';
import Signup from './pages/Register';
import Logout from './pages/Logout';
import Admin from './pages/Admin';
import AddProduct from './pages/AddProduct';  
import Error from './pages/Error';
import { UserProvider } from './UserContext';
import { Route, Switch, BrowserRouter as Router } from 'react-router-dom';
function App() {

  const [user, setUser] = useState({
    id: null,
    isAdmin: null
  });

  const unsetUser = () => {

    localStorage.clear(); 

    setUser({
      id: null,
      isAdmin: null
    });

  }

  useEffect(() => {

    console.log(user);

    fetch(`https://stormy-savannah-08197.herokuapp.com/users/details`, {
      headers: {
        Authorization: `Bearer ${ localStorage.getItem('accessToken') }` 
      } 
    }).then(resultOfPromise => resultOfPromise.json()).then(convertedResult => {
      console.log(convertedResult);
      if (convertedResult._id !== "undefined") {
          setUser({
            id: convertedResult._id,
            isAdmin:convertedResult.isAdmin
          });
      } else {
          setUser({
            id: null,
            isAdmin: null
          });
      }
    })

  },[]);

  return (
    <UserProvider value={{user, unsetUser, setUser}}>
      <Router>
        <Navbar />

        <Switch>
          <Route exact path="/" component={Landing} />
          <Route exact path="/courses" component={Courses} />
          <Route exact path="/login" component={Login} />
          <Route exact path="/admin" component={Admin} />
          <Route exact path="/createproduct" component={AddProduct} />
          <Route exact path="/register" component={Signup} />
          <Route exact path="/logout" component={Logout} />
          <Route exact path="/courses/:courseId" render={(props => <CourseView {...props} />)} />
          <Route component={Error} />
        </Switch>

        {/*<Footer />*/}
      </Router>
    </UserProvider>
  );
}

export default App;

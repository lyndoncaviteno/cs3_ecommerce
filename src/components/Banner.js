import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';

function Banner({kahitAno}) {

	const { title, tagline } = kahitAno;


	return(
		<Row>
			<Col className="p-5">
				<h1> {title} </h1>
				{/*<p> {tagline} </p>*/}
				<p> {tagline} </p>
			</Col>
		</Row>
	)
}

export default Banner;

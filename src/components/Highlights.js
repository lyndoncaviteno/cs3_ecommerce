import { Row, Col, Card } from 'react-bootstrap'

export default function Highlights() {
	return(
		<Row className="mt-3 mb-3">
		{/*first column*/}
			<Col xs={12} md={4}>
				<Card className="Highlights p-3">
					<Card.Body>
						<Card.Title>
							Hassle-Free Transaction
						</Card.Title>
							<Card.Text>

							</Card.Text>
					</Card.Body>
				</Card>
			</Col>
		{/*second column*/}
			<Col xs={12} md={4}>
				<Card className="Highlights p-3">
					<Card.Body>
						<Card.Title>
							New Kicks Every Week
						</Card.Title>
							<Card.Text>
								
							</Card.Text>
					</Card.Body>
				</Card>
			</Col>
		{/*third column*/}
			<Col xs={12} md={4}>
				<Card className="Highlights p-3">
					<Card.Body>
						<Card.Title>
							Guaranteed Original
						</Card.Title>
							<Card.Text>
								
							</Card.Text>
					</Card.Body>
				</Card>
			</Col>
		</Row>
	);
}

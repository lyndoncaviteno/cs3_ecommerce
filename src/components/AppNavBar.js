import { useContext } from 'react'
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';
import { NavLink } from 'react-router-dom';
import UserContext from '../UserContext'

const RegisteredLinks = ({isAdmin}) => <>
	<Nav.Link as={NavLink} to="/" >Home</Nav.Link>
	<Nav.Link as={NavLink} to="/courses">Products</Nav.Link>
	{ isAdmin && <Nav.Link as={NavLink} to="/admin">Admin</Nav.Link> }
	<Nav.Link as={NavLink} to="/logout">Logout</Nav.Link>
</>

const UnregisteredLink = () => <>
		<Nav.Link as={NavLink} to="/register" >Join Us</Nav.Link>
		<Nav.Link as={NavLink} to="/login" >Sign In</Nav.Link>
</>

function AppNavBar() {

	const { user } = useContext(UserContext);

	return (
		<Navbar bg="light" expand="lg">
			<Navbar.Brand>
				ShoeBiz
			</Navbar.Brand>
			<Navbar.Toggle aria-controls="basic-navbar-nav" />
			<Navbar.Collapse id="basic-navbar-nav">
				
				{(user.id) ?
					<RegisteredLinks isAdmin={user.isAdmin} />
				   :
				   	<UnregisteredLink />
				}

			</Navbar.Collapse>
		</Navbar>
	)
};

export default AppNavBar;

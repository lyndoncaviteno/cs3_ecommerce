import Card from 'react-bootstrap/Card';
import { Link } from 'react-router-dom';

export default function CourseCard({courseInfo}) {

const { _id, name, description, price } = courseInfo;
	
	return (
		<Card>
			<Card.Body>
				<Card.Title>{name}</Card.Title>
				<Card.Subtitle>Product Description:</Card.Subtitle>
				<Card.Text>
					{description}
				</Card.Text>
				<Card.Subtitle>Price:</Card.Subtitle>
				<Card.Text>P {price} </Card.Text>
				<Link className="btn btn-primary" to={`/courses/${_id}`}> See Details </Link>
			</Card.Body>
		</Card>
	);
}
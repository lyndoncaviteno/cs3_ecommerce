import { useEffect, useState } from 'react';
import Hero from '../components/Banner';
import SubjectCard from '../components/CourseCard';

let info = {
	title: "Welcome to the Products Page!",
	tagline: "New releases every week!"
}

export default function Courses() {

	const [courses, setCourses] = useState([]); 

	useEffect(() => {

       fetch('https://still-wave-85902.herokuapp.com/products/activeProducts').then(response => response.json()).then(convertedData => {
       		console.log(convertedData);

       		setCourses(convertedData.map(subject => {
       			return(

					<SubjectCard key={subject._id} courseInfo={subject}/>
       			)
       		}))
       	})
      })

	return(
		<>	
			
			<Hero kahitAno={info}/>
			{courses}
		</>
	);
}
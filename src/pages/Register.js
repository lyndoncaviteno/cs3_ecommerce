import { useState, useEffect } from 'react';
import { Form, Button, Container } from 'react-bootstrap';
import Hero from '../components/Banner';
import Swal from 'sweetalert2';
//import img from '../aj1.png';
import {Redirect, useHistory} from 'react-router-dom';

const paraSaBanner = {
	title: "Become a ShoeBiz Member",
	tagline: "Create your account to get first access to ShoeBiz products."
}

export default function Register() {

	const history = useHistory();
	const [ firstName, setFirstName ] = useState('');
	const [ middleName, setMiddleName ] = useState('');
	const [ lastName, setLastName ]= useState('');
	const [ email, setEmail ] = useState('');
	const [ password1, setPassword1 ] = useState('');
	const [ gender, setGender ] = useState('Male');
	const [ password2, setPassword2 ] = useState('');
	const [ mobile, setMobile ] = useState('');
	const [ isRegisterBtnActive, setRegisterBtnActive ] = useState('');
	const [ isComplete, setIsComplete ]= useState(false);
	const [ isMatched, setIsMatched ]= useState(false);

	function registerUser(event) {
	      event.preventDefault(); 

	    console.log(firstName);
	    console.log(middleName);
	    console.log(lastName);
	    console.log(email);
	    console.log(mobile);
	    console.log(password1);
	    console.log(gender);

	      fetch(`https://still-wave-85902.herokuapp.com/users/register`, {
	      	method: "POST", 
	      	headers: {
	      		'Content-Type': 'application/json' 
	      	},
	      	body: JSON.stringify({
	      		firstName: firstName,
	      		middleName: middleName,
	      		lastName: lastName,
	      		email: email,
	      		password: password1,
	      		mobileNo: mobile,
	      		gender: gender
	      	})
	      })
	      .then(res => res.json()) 
	      .then(data => { 
	      	
	      if (data !== undefined ) {
		      Swal.fire({
		        title: `You have registered successfully!`,
		        icon: 'success',
		        text: 'Welcome to ShoeBiz!'
		      });

	      history.push('/login');
	      
	      } else {
		      	Swal.fire({
		      	  title: `Register Unsuccessful`,
		      	  icon: 'error',
		      	  text: 'Please register again.'
		      	});
	      }
	    }) 
	} 

	useEffect(() => { 

		console.log(gender)
		if ((firstName !== '' && middleName !== '' && lastName !== '' && email !== '' && password1 !== '' && password2 !== '' && mobile !== '') && (password1 === password2)) {
			setRegisterBtnActive(true);
			setIsComplete(true);
			setIsMatched(true);
			//console.log("Hello");
		} else {
			setRegisterBtnActive(false);
			setIsComplete(false);
			setIsMatched(false);
			//console.log("Hi");
		}
	},[firstName, middleName, lastName, email, password1, password2, mobile]); 
	
	useEffect(() => {
		if (password1 === '' && password2 === '') {
			setIsMatched('empty');
		} else if (password1 === password2) {
			setIsMatched(true);
		} else {
			setIsMatched(false);
		}
	}) 
	

	return(
		<Container>
			<div className="banner-text">
				{/*Banner/Greetings*/}
				<Hero kahitAno={paraSaBanner}/>
			</div>
			{/*Find a way to make this component "reactive*/}
			<div className="form-box">
			{
				isComplete ? 
				<h1 className="mb-5"> Proceed with Register! </h1>
				:
				<h1 className="mb-5"> Fill out the form below: </h1>
			}
				{/*Register Form*/}
				<Form onSubmit={(e) => registerUser(e) }className="mb-5">
				{/*First Name*/}
					<Form.Group controlId="firstName">
						<Form.Label>
							First Name:
						</Form.Label>
						<Form.Control type="text" 
						placeholder="Insert First Name Here" 
						value={firstName} 
						onChange={event => setFirstName(event.target.value)} 
						required
						/>
					</Form.Group>

				{/*Middle Name*/}
					<Form.Group controlId="middleName">
						<Form.Label>
							Middle Name:
						</Form.Label>
						<Form.Control type="text" 
						placeholder="Insert Middle Name Here"
						value={middleName}
						onChange={event => setMiddleName(event.target.value)} 
						required
						/>
					</Form.Group>

				{/*Last Name*/}
					<Form.Group controlId="lastName">
						<Form.Label>
							Last Name:
						</Form.Label>
						<Form.Control type="text" 
						placeholder="Insert Last Name Here" 
						value={lastName}
						onChange={event => setLastName(event.target.value)}
						required
						/>
					</Form.Group>

				{/*Email*/}
					<Form.Group controlId="email">
						<Form.Label>
							Email Address:
						</Form.Label>
						<Form.Control type="userEmail" 
						placeholder="Insert Email Here" 
						value={email}
						onChange={event => setEmail(event.target.value)}
						required
						/>
					</Form.Group>

				{/*Password*/}
					<Form.Group controlId="password1">
						<Form.Label>
							Password:
						</Form.Label>
						<Form.Control type="password" 
						placeholder="Insert Password Here" 
						value={password1}
						onChange={event => setPassword1(event.target.value)}
						required
						/>
					</Form.Group>
					{
						isMatched === 'empty' ?
						<p></p>
						:
						isMatched
						?
						<p className="text-success">*Passwords matched.*</p>
						:
						<p className="text-danger">*Passwords should match.*</p>
					}

				{/*Verify Password*/}
					<Form.Group controlId="password2">
						<Form.Label>
							Verify Password:
						</Form.Label>
						<Form.Control type="password" 
						placeholder="Verify Password Here" 
						value={password2}
						onChange={event => setPassword2(event.target.value)} 
						required
						/>
					</Form.Group>	

				{/*Gender*/}
				{/*let's create a predetermined response for the gender*/}
					<Form.Group controlId="gender">
						<Form.Label>
							Gender:
						</Form.Label>
					<Form.Control as="select" value={gender} onChange={(event) => setGender(event.target.value)}>
							<option selected disabled>Select Gender Here</option>
							<option value="Male">Male</option>
							<option value="Female">Female</option>
						</Form.Control>
					</Form.Group>

				{/*Mobile Number*/}
					<Form.Group controlId="mobile">
						<Form.Label>
							Mobile Number:
						</Form.Label>
						<Form.Control type="number" 
						placeholder="Insert Mobile Number Here" 
						value={mobile}
						onChange={event => setMobile(event.target.value)}
						required
						/>
					</Form.Group>
						{isRegisterBtnActive ? 
							<Button variant="success" className="btn btn-block" type="submit" id="submitBtn">
								Create New Account
							</Button>
							: 
							<Button variant="danger" className="btn btn-block" type="submit" id="submitBtn" disabled>
								Create New Account
							</Button>
							}

				</Form>
			</div>
		</Container>
	);
}
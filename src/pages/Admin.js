import { useEffect, useState } from "react";
import { Table, Modal, Button, Form } from 'react-bootstrap';
import AddProduct from '../pages/AddProduct';
import Nav from 'react-bootstrap/Nav';
import { NavLink } from 'react-router-dom';

const info = {
    title: "This is the admin Page.",
    subtitle: "Welcome"
}


export default function Admin() {
    
    const [products, setProducts] = useState([]);
    
    const [show, setShow] = useState(false);
    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);

    useEffect((
        ) => {

            fetch('https://still-wave-85902.herokuapp.com/products')
            .then(response => response.json())
            .then(convertedData => {
                setProducts(convertedData);
        })
    },[])
    
    return(
        <div>
            {info.title}
            <>
            <Nav.Link as={NavLink} to="/createproduct">Create Product</Nav.Link>
            </>
            <Table striped bordered hover>
                <thead>
                    <tr>
                    <th>Name</th>
                    <th>Description</th>
                    <th>Price</th>
                    <th>Availability</th>
                    </tr>
                </thead>
                <tbody>
                    {products.map(item => (
                        <tr>
                            <td>
                                {item.name}
                            </td>
                            <td>
                                {item.description}
                            </td>
                            <td>
                                {item.price}
                            </td>
                            <td>
                                {item.isActive ? "Available" : "Unavailable"}
                            </td>
                        </tr>
                    ))}
                </tbody>
            </Table>
        </div>
        )
}


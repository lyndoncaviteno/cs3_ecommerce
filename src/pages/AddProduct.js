import React from 'react';
import { Modal, Form, Button } from 'react-bootstrap';
import { useState, useEffect } from 'react';
import Swal from 'sweetalert2';
import {Redirect, useHistory} from 'react-router-dom';


const NewProduct = ({show, handleClose}) => {
    const history = useHistory();
    const [ name, setName ] = useState('');
    const [ description, setDescription ] = useState('');
    const [ price, setPrice ] = useState('')

    function addProduct (event) {
        event.preventDefault()

        console.log(name);
        console.log(description);
        console.log(price)

        const token = window.localStorage.getItem('theAccessToken');
        fetch(`https://still-wave-85902.herokuapp.com/products/create`, {

            method: "POST", 
	      	headers: {
	      		'Content-Type': 'application/json',
                  'Authorization': `Bearer ${token}` 
                },
                body: JSON.stringify({
                    name: name,
                    description: description,
                    price: price
                })
            })
            .then(res => {
                console.log(res)
                return res.json()}) 
                .then(data => { 
                    if (data !== undefined ) {
                        Swal.fire({
                            title: `Product Added Successfully`,
                            icon: 'success'
                        });

                        history.push('/admin');
                        
                        
                    } else {
		      	Swal.fire({
		      	  title: `Product Was Not Added`,
		      	  icon: 'error'
		      	});
	      }
	    }) 
	} 

    return(
        
    <div>
        <h1>Create Product</h1>
            <Form onSubmit={(event) => addProduct(event)}>
                <div>
                    <Form.Group className="mb-3" controlId="">
                        <Form.Label>Name</Form.Label>
                        <Form.Control type="text" value={name} onChange={event => setName(event.target.value)}placeholder="Product Name" />
                    </Form.Group>

                    <Form.Group className="mb-3" controlId="">
                        <Form.Label>Description</Form.Label>
                        <Form.Control as="textarea" rows={3} value={description} onChange={event => setDescription(event.target.value)} placeholder="Product Description"/>
                    </Form.Group>
                    
                    <Form.Group className="mb-3" controlId="">
                        <Form.Label>Price</Form.Label>
                        <Form.Control type="number" value={price} onChange={event => setPrice(event.target.value)} placeholder="Value" />
                    </Form.Group>

                    {/* <Form.Group className="mb-3" controlId="exampleForm.ControlTextarea1">
                        <Form.Label>Example textarea</Form.Label>
                        <Form.Control as="" rows={3} />
                    </Form.Group> */}
                </div>
                
                <div>   
                    <Button variant="secondary" onClick={handleClose}>
                        Close
                    </Button>
                    
                    <Button variant="primary" type="submit">
                        Add Product
                    </Button>
                </div>
            </Form>
    </div>
    )
}

export default NewProduct;
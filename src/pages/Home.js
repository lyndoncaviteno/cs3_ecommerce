import Hero from '../components/Banner'
import Showcase from '../components/Highlights'
/*import {Fragment} from 'react';*/
import Container from 'react-bootstrap/Container'

let details = {
	title: "Welcome to ShoeBiz!",
	tagline: "Your one-stop shop for all hyped sneakers!",
}

export default function Home() {
	return (
	<Container>
		<Hero kahitAno={details} />
		<Showcase />
	</Container>
	);
}


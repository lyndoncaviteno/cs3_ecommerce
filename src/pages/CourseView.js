import { Container, Card, Button, Row, Col } from 'react-bootstrap';
import UserContext from '../UserContext';
import { useState, useEffect, useContext } from 'react';

export default function ProductView(props) {
	const [product, setProduct] = useState();

	useEffect(async () => {

		let response = await fetch(`https://still-wave-85902.herokuapp.com/products/${props.match.params.courseId}`)
		let result = await response.json()
		setProduct(result);
	
	}, []);

	const name = product ? product.name : ""
	const description = product ? product.description : ""
	const price = product ? product.price : ""

	return (
		<Container className="mt-5">
			<Row>
				<Col lg={{ span: 6, offset: 3 }}>
					<Card>
						<Card.Body className="text-center">
							<Card.Title> {name} </Card.Title>
							<Card.Subtitle>Description:</Card.Subtitle>
							<Card.Text> {description} </Card.Text>
							<Card.Subtitle>Price:</Card.Subtitle>
							<Card.Text> {price} </Card.Text>

							<Card.Text></Card.Text>

							<Button variant="primary" block>Add to Cart</Button>



						</Card.Body>
					</Card>
				</Col>
			</Row>
		</Container>
	)
}
import UserContext from '../UserContext';
import { useState, useEffect, useContext } from 'react';
import { Redirect } from 'react-router-dom';
import { Form, Button, Container } from 'react-bootstrap';
import Hero from '../components/Banner';
import Swal from 'sweetalert2';

const bannerLabel = {
	title: "Login to your account.",
	tagline: "Access your account and start enrolling to your desired course."
}

export default function Login() {

	const { user, setUser } = useContext(UserContext)

	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');
	const [isActive, setIsActive] = useState(false);

	const authenticate = (event) => {
		event.preventDefault()

		fetch(`https://still-wave-85902.herokuapp.com/users/login`, {
			method: "POST",
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				email: email,
				password: password
			})
		})
			.then(res => res.json())
			.then(ConvertedInformation => {
				console.log(ConvertedInformation)

				if (ConvertedInformation.accessToken !== "undefined") {

					localStorage.setItem('theAccessToken', ConvertedInformation.theAccessToken);

					retrieveUserDetails(ConvertedInformation.theAccessToken, ConvertedInformation.userId);
					Swal.fire({
						title: "Logged In Successfully",
						icon: "success",
						text: "Welcome to ShoeBiz!"
					});
				} else {

					Swal.fire({
						title: "Authentication Failed",
						icon: "error",
						text: "Check your login details and try again."
					});
				}
			})
	}

	const retrieveUserDetails = (token, userId) => {

		fetch(`https://still-wave-85902.herokuapp.com/users/details/${userId}`, {
			headers: {
				Authorization: `Bearer ${token}`
			}
		})
			.then(res => res.json())
			.then(convertedResult => {

				console.log(convertedResult);
				setUser({
					id: convertedResult[0]._id,
					isAdmin: convertedResult[0].isAdmin
				});
			})
			.catch(e => {
				console.log(`Error: ${e}`)
				throw (e)
			})
	}

	useEffect(() => {
		if (email !== "" && password !== "") {
			setIsActive(true)
		} else {
			setIsActive(false)
		}
	}, [email, password]);

	if (user && user.id) {

		return <Redirect to="/courses" />
	}

	return (

		<Container>
			{/*Banner of the Page*/}
			<Hero kahitAno={bannerLabel} />

			{/*Login Form Component*/}
			<Form className="mb-5" onSubmit={event => authenticate(event)}>
				{/*User's Email*/}
				<Form.Group controlId="userEmail">
					<Form.Label>Email Address:</Form.Label>
					<Form.Control
						type="email"
						placeholder="Insert Email Here"
						value={email}
						onChange={event => setEmail(event.target.value)}
						required />
				</Form.Group>

				{/*User's Password*/}
				<Form.Group controlId="userPassword">
					<Form.Label>Password:</Form.Label>
					<Form.Control
						type="password"
						placeholder="Insert Password Here"
						value={password}
						onChange={event => setPassword(event.target.value)}
						required />
				</Form.Group>

				{/*Button Component*/}
				{
					isActive ?
						<Button type="submit" id="submitBtn" variant="success" className="btn btn-block">Login</Button>
						:
						<Button type="submit" id="submitBtn" variant="success" className="btn btn-block" disabled>Login</Button>
				}
			</Form>
		</Container>
	);
}
